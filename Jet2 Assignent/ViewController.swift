//
//  ViewController.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/12/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
//  Home View controller

// MARK: - Import

import UIKit

// MARK: - Class

class ViewController: UIViewController {

    // MARK: - Outlet
    
    @IBOutlet private weak var articleTableView: UITableView!
    
    // MARK: - Variables
    
    private var viewModel = HomeViewModel()
    private var isFetchingMore: Bool = false
    private var isPagingCompleted: Bool = false
    private var currentPage: Int = 1
    
    // MARK: - Life cycle Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Elements().addActivityIndicatorOn(navigationController: self.navigationController)
        self.setupCell()
        self.setupNavigationBar()
        self.navigationController?.title = NSLocalizedString("ArticlesTitle", comment: "Articles Title") 
        self.fetchDataForCurrentPage()
    }
    
    // MARK: - Helper Method
    
    // * Fetch Data For CurrentPage *
    private func fetchDataForCurrentPage() {

        self.articleTableView.reloadSections(IndexSet(integer: 1), with: .none)
        HomeViewModel.fetchArticle(currentPage: self.currentPage) { (response, error) in
            Elements().dismissActivityIndicator()
            
            if let response = response {
                if !response.data.isEmpty {
                    response.data.forEach { (element) in
                        self.viewModel.data.append(element)
                    }
                    self.isFetchingMore = false
                    self.articleTableView.reloadData()
                } else {
                    self.isPagingCompleted = true
                    self.articleTableView.reloadSections(IndexSet(integer: 1), with: .none)
                    
                }
            } else if let err = error {
                
                let alert = UIAlertController(title: NSLocalizedString("ServiceFailureAlertTitleKey", comment: "fetch Data For CurrentPage"), message: err.localizedDescription, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Reload?", style: .default, handler: { (action) in
                    self.isPagingCompleted = true
                    self.fetchDataForCurrentPage()
                }))
                self.present(alert, animated: true, completion: nil)
                print(err)
            }
        }
    }
    
    
    // * Register and setup all the cell used *
    private func setupCell() {
        self.articleTableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "articleCell")
        self.articleTableView.register(UINib(nibName: "LoaderTableViewCell", bundle: nil), forCellReuseIdentifier: "loaderTableViewCell")
        self.articleTableView.register(UINib(nibName: "CentreTextTableViewCell", bundle: nil), forCellReuseIdentifier: "centreCellTableCell")
    }
    
    
    // * Setup properties for the navigation bar *
    private func setupNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = #colorLiteral(red: 0.2998246409, green: 0.9709281025, blue: 0.8032460257, alpha: 1)
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.prefersLargeTitles = true
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        } else {
            self.navigationController?.navigationBar.tintColor = #colorLiteral(red: 0.2998246409, green: 0.9709281025, blue: 0.8032460257, alpha: 1)
        }
    }

}

// MARK: - UITableViewDelegate

extension ViewController: UITableViewDelegate {
    // Add your delegate methods here for UITableView
}

// MARK: - UITableViewDelegate

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return self.viewModel.data.count
        } else if section == 1 && self.isFetchingMore {
            return 1
        }
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "articleCell") as? ArticleTableViewCell else { return UITableViewCell() }
            cell.initialize(data: viewModel.data[indexPath.row], tableView: tableView, index: indexPath)
            return cell
        } else {
            if !isPagingCompleted {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "loaderTableViewCell") as? LoaderTableViewCell else { return UITableViewCell() }
                cell.spinner.startAnimating()
                return cell
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "centreCellTableCell") as? CentreTextTableViewCell else { return UITableViewCell() }
                cell.initializeView(cellType: .pagingCompletion)
                return cell
            }
        }
    }
}

// MARK: - UIScrollViewDelegate

extension ViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let scrollViewHeight = scrollView.frame.size.height;
        let scrollContentSizeHeight = scrollView.contentSize.height;
        let scrollOffset = scrollView.contentOffset.y;
        if scrollOffset > scrollContentSizeHeight - scrollViewHeight, !self.isFetchingMore
        {
            self.isFetchingMore = true
            self.currentPage += 1
            self.fetchDataForCurrentPage()
            
        }
    }
}
