//
//  ArticleTableViewCell.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/13/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
//  Article Cell

// MARK: - Import

import UIKit

// MARK: - Class

class ArticleTableViewCell: UITableViewCell {
    
    // MARK: - Variables
    @IBOutlet private weak var profileImageView: CustomImageViewWithCache!
    @IBOutlet private weak var profileUserName: UILabel!
    @IBOutlet private weak var profileUserDesignation: UILabel!
    @IBOutlet private weak var articleImage: CustomImageViewWithCache!
    @IBOutlet private weak var articleContent: UILabel!
    @IBOutlet private weak var articleTitle: UILabel!
    @IBOutlet private weak var articleURLString: UILabel!
    @IBOutlet private weak var commentsCountLabel: UILabel!
    @IBOutlet private weak var likesCountLabel: UILabel!
    @IBOutlet private weak var timeElapsedLabel: UILabel!
    
    // MARK: - Helper Method
    
    // * Setup the cell according to the cell data you are passing *
    // @param:
    //      data: Cell data as MainElement to set the cell
    //      tableView: Parent tableView to refresh the cells if needed
    //      index: Parent tableView cell index to refresh the cells if needed
    func initialize(data: MainElement?, tableView: UITableView, index: IndexPath) {
        guard let data = data else { return }
        self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2
        self.profileImageView.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        self.profileImageView.layer.borderWidth = 1
        if let urlString = data.user.first?.avatar {
            self.profileImageView.loadImageUsingUrlString(urlString, tableView: tableView, indexpath: index)
        } else {
             self.profileImageView.image = nil
        }
        if let urlString = data.media.first?.image {
            self.articleImage.loadImageUsingUrlString(urlString, tableView: tableView, indexpath: index)
        } else {
            self.articleImage.image = nil
        }
        self.profileUserName.text = data.user.first?.name
        self.profileUserDesignation.text = data.user.first?.designation
        self.timeElapsedLabel.text = HomeViewModel().getDifferenceWRTPresent(dateForArticle: data.createdAt)
        self.likesCountLabel.text = "\(data.likes) Likes"
        self.commentsCountLabel.text = "\(data.comments) Comments"
        self.articleContent.text = data.content
        self.articleTitle.text = data.media.first?.title
        self.articleURLString.text = data.media.first?.url
        
    }
    
}
