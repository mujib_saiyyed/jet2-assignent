//
//  HomeViewModel.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/12/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//

import Foundation

class HomeViewModel {
    
    // MARK: - Constant
    var data = MainModel()
    
     // MARK: - Typealias
    typealias CompletionHandler = (HomeViewModel?, Error?) -> Void
    
    // MARK: - Method
    
    // * Fetch articles *
    // @param:
    //      currentPage: Current page number for paging
    //      completionHandler: Completion handler for the service as (HomeViewModel?, Error?) -> Void
    class func fetchArticle (currentPage: Int, completionHandler: @escaping CompletionHandler ) {
        ArticlesService().fetchArticles(currentPage: currentPage) { (response, error) in
            if let result = response {
                let homeModelView = HomeViewModel()
                homeModelView.data = result
                completionHandler(homeModelView, nil)
            } else if let err = error {
                completionHandler(nil, err)
            }
        }
    }
    
    func getDifferenceWRTPresent(dateForArticle: String) -> String {
        let cal = Calendar.current
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_IN")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        dateFormatter.isLenient = true
        let d1 = Date()
        if let d2 = dateFormatter.date(from: dateForArticle) {
            let components = cal.dateComponents([.hour, .minute, .day], from: d2, to: d1)
            
            if let days = components.day,
                days > 1 {
                return "\(days) days ago"
            } else {
                if let hours = components.hour,
                    hours >= 1 {
                    return "\(hours) hours ago"
                } else if let min = components.minute {
                    if min > 1 {
                        return "\(min) minutes ago"
                    } else {

                        return "\(min) minute ago"
                    }
                }
            }
        }
        
        return dateForArticle
    }
}
