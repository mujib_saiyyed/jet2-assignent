//
//  ArticlesService.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/12/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
// Use this service class to fetch the articles according to the page number passed.

// MARK: - Imports

import Alamofire

// MARK: - Class

class ArticlesService {
    
    // MARK: - Typealias
    
    typealias CompletionHandler = (MainModel?, Error?) -> Void
    
    // MARK: - Constant
    
    private static let urlString: String = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/blogs"
    
    // MARK: - Method
    
    // * Fetch articles *
    // @param:
    //      currentPage: Current page number for paging
    //      completionHandler: Completion handler for the service as (MainModel?, Error?) -> Void
    func fetchArticles(currentPage: Int, completionHandler: @escaping CompletionHandler) {
        let selfType = type(of: self)
        
        // Uncomment this and comment below Alamofire code if you want system methods to fetch the service data.
        
        //        if let url = URL(string: selfType.urlString) {
        //            let request = URLRequest(url: url)
        //            URLSession.shared.dataTask(with: request) { data, res, error in
        //
        //                if let response = try? JSONSerialization.jsonObject(with: data!, options: []) {
        //                    print(response)
        //                }
        //                if let data = data {
        //                    if let json = try? JSONDecoder().decode(MainModel.self, from: data) {
        //                        print(json)
        //                    }
        //                }
        //            }.resume()
        //        }
        
        var params: [String: Any] = [:]
        params["page"] = currentPage
        params["limit"] = 10
        
        AF.request(selfType.urlString, method: .get, parameters: params).responseData { (response) in
            print(response.description)
            switch response.result {
            case .success(let data):
                if let json = try? JSONDecoder().decode(MainModel.self, from: data) {
                    print(json)
                    completionHandler(json, nil)
                }
            case .failure(let err):
                print(err)
                completionHandler(nil, err)
            }
        }
    }
}
