//
//  LoaderTableViewCell.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/14/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
// TableView Cell with loader/spinner/activity Indicator. Just register with identifier and startAnimation for the spinner

// MARK: - Imports

import UIKit

// MARK: - Class

class LoaderTableViewCell: UITableViewCell {

    // MARK: - Outlet
    
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    // MARK: - Lifecycle Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Do some additional stuff here
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
