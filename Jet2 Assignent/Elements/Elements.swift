//
//  Elements.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/14/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
// Generic class to add or remove elements. Added additional elements to be used throughout the application.

// MARK: - Imports

import Foundation
import UIKit

// MARK: - Class

class Elements: NSObject {
    
    // MARK: - Variables
    
    private var navigatorFrame: CGRect!
    private var loaderView: UIView {
        let view = UIView(frame: self.navigatorFrame)
        view.alpha = 0.7
        view.backgroundColor = .black
        return view
    }
    private var spinner: UIActivityIndicatorView {
        let spinnerView = UIActivityIndicatorView(frame: self.navigatorFrame)
        spinnerView.style = .large
        spinnerView.color = .systemPink
        spinnerView.hidesWhenStopped = true
        spinnerView.isHidden = false
        spinnerView.startAnimating()
        return spinnerView
        
    }
    private static var views: [UIView] = []
    
    // MARK: - Loader/Indicator Methods
    
    // *Add activity indicator on your navigation controller*
    // @param: navigationController: Navigation controller you are working on
    func addActivityIndicatorOn(navigationController: UINavigationController?) {
        guard let navigationView = navigationController?.view else { return }
        self.navigatorFrame = navigationView.frame
        let loader = self.loaderView
        let spinner = self.spinner
        navigationView.addSubview(loader)
        navigationView.addSubview(spinner)
        type(of: self).views.append(spinner)
        type(of: self).views.append(loader)
    }
    
    // *Remove activity indicator from your navigation controller*
    
    func dismissActivityIndicator() {
        let selfType = type(of: self)
        selfType.views.forEach { (view) in
            view.removeFromSuperview()
        }
        selfType.views.removeAll()
    }
    
}
