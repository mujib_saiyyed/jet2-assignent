//
//  CentreTextTableViewCell.swift
//  Jet2 Assignent
//
//  Created by Mujib Personal  on 7/15/20.
//  Copyright © 2020 Mujib Personal . All rights reserved.
//
// Use this generic cell with bold text in the centre. Just pass in the cell type  to get predefined text or a string to set for the label.

// MARK: - Imports

import UIKit

// MARK: - Enum

//Use this enum for cellType to get the predefined text. Add your own cases here to add extra functionality to this.

enum CellType: String {
    case pagingCompletion = "Thats all Folks!"
}

// MARK: - Class

class CentreTextTableViewCell: UITableViewCell {
    
    // MARK: - Outlet
    
    @IBOutlet private weak var centreTextLabel: UILabel!
    
    // MARK: - Initializing methods
    
    // * Setup the text according to the cell type you are using *
    // @param: cellType: Cell type from enum CellType to set predefined text
    func initializeView(cellType: CellType) {
        
        self.centreTextLabel.text = CellType.pagingCompletion.rawValue
        
    }
    
    // * Setup the text according to the cell type you are using *
    // @param: cellText: Cell text to set predefined text
    func initializeView(cellText: String) {
        
        self.centreTextLabel.text = cellText
        
    }
    
}
